﻿using System;
using System.Collections.Generic;
using System.Linq;
using FbB2bCustomAlert.Models;
using FbB2bCustomAlert.Helpers;

namespace FbB2bCustomAlert.Services
{
    class ReconciliationServiceLinkedTanks
    {
        private readonly string _siteId;
        private readonly string _ownerId;
        private string _tankIds;
        private List<string> _listTankIds;
        private double worstVar;
        private List<SalesTransaction> _listSalesTransactionsByTank;
        private List<ReconciliationResult> _listReconciliationResult;
        private readonly DateTime _recDateTimeUTC;
        private FuelType _brand;

        private TestPeriodLinkedTanks _testPeriod1;
        private TestPeriodLinkedTanks _testPeriod2;
        private TestPeriodLinkedTanks _testPeriod3;

        private DateTime _earliestStartDateTime;
        private List<DateTime> _listEarliestStartDateTime;

        IEnumerable<StockReading> stockReading;
        IEnumerable<StockReading> lastSL;

        private List<double> _listExistingVariances;

        private SalesTransaction _latestSalesTransaction;
        private readonly string _connectionString;

        public ReconciliationServiceLinkedTanks(string connectionString, string siteId, string ownerId, string tankIds, DateTime recDateTimeUTC)
        {
            _siteId = siteId;
            _ownerId = ownerId;
            _tankIds = tankIds;
            _listTankIds = new List<string>();
            _listTankIds = ConvertCommaDelimitedString.ToListOfTypeString(tankIds);

            _listSalesTransactionsByTank = new List<SalesTransaction>();
            _listReconciliationResult = new List<ReconciliationResult>();
            _latestSalesTransaction = new SalesTransaction();

            _recDateTimeUTC = recDateTimeUTC;
            _testPeriod1 = new TestPeriodLinkedTanks();
            _testPeriod2 = new TestPeriodLinkedTanks();
            _testPeriod3 = new TestPeriodLinkedTanks();

            _earliestStartDateTime = new DateTime();
            _listEarliestStartDateTime = new List<DateTime>();
            _connectionString = connectionString;
        }

        /*
         * This is for multiple tanks - 3 time periods
         */

        public ReconciliationResult Run()
        {

            using (var dbService = new DBService(_connectionString))
            {
                lastSL = dbService.GetLastStockReadingMultipleTanks(_siteId, _ownerId, _tankIds, _recDateTimeUTC);

                if (lastSL.Count() == 0)
                    throw new StockReadingNotFoundException("Stock readings for the specified tank not found. Reconciliation aborted.");

                //Latest brand for tank
                //ToDo get brand for linked tanks
                _brand = dbService.GetFuelGrade(_siteId, _ownerId, ConvertCommaDelimitedString.ToListOfTypeString(_tankIds).FirstOrDefault(), _recDateTimeUTC).FirstOrDefault();

                IEnumerable<StockReading> listOfStockReadings = dbService.GetStockReadingsForMultipleTanks(_siteId, _ownerId, _tankIds, _recDateTimeUTC);

                IEnumerable<StockReading> timePeriodLinkedTanksS1 = listOfStockReadings.Where(s => s.Period == "s1"); //Tanks 1,2,3
                IEnumerable<StockReading> timePeriodLinkedTanksS2 = listOfStockReadings.Where(s => s.Period == "s2"); //Tanks 1,2,3
                IEnumerable<StockReading> timePeriodLinkedTanksS3 = listOfStockReadings.Where(s => s.Period == "s3"); //Tanks 1,2,3
                IEnumerable<StockReading> timePeriodLinkedTanksE1 = listOfStockReadings.Where(s => s.Period == "e1"); //Tanks 1,2,3

                _tankIds = _tankIds.Replace("'", "");
                _tankIds = _tankIds.Replace(",", ", ");

                _testPeriod1.TankId = _tankIds;
                _testPeriod2.TankId = _tankIds;
                _testPeriod3.TankId = _tankIds;

                //S1
                _testPeriod1.SetStartValues(timePeriodLinkedTanksS1);
                _testPeriod2.SetEndValues(timePeriodLinkedTanksS1);

                //S2
                _testPeriod2.SetStartValues(timePeriodLinkedTanksS2);
                _testPeriod3.SetEndValues(timePeriodLinkedTanksS2);

                //S3
                _testPeriod3.SetStartValues(timePeriodLinkedTanksS3);

                //E1
                _testPeriod1.SetEndValues(timePeriodLinkedTanksE1);

                //Add sales transactions
                if (_testPeriod1.TankId != null)
                    _listEarliestStartDateTime.Add(_testPeriod1.StartUTC.Value);

                if (_testPeriod2.TankId != null)
                    _listEarliestStartDateTime.Add(_testPeriod2.StartUTC.Value);

                if (_testPeriod3.TankId != null)
                    _listEarliestStartDateTime.Add(_testPeriod3.StartUTC.Value);

                _earliestStartDateTime = _listEarliestStartDateTime.Select(s => s).Min();

                //Can't continue if we have no start or no end point
                //if (_t1.EndUTC == default(DateTime?))
                //  throw new StockReadingNotFoundException("Stock reading for the last reconcile period not found. Reconciliation aborted.");

                //if (_t3.StartUTC == default(DateTime?))
                //   throw new StockReadingNotFoundException("Stock reading for the first reconcile period not found. Reconciliation aborted.");

                //If there are insufficient records we can just use the next record (may result in a zero-length period)
                //No stock movement may result in insufficient records.
                //if (_t1.StartUTC == default(DateTime?)) _t1.SetStartValues(_t2);

                //if (_t2.StartUTC == default(DateTime?)) _t2.SetStartValues(_t3);
                //if (_t2.EndUTC == default(DateTime?)) _t2.SetEndValues(_t3);
                //if (_t3.EndUTC == default(DateTime?)) _t3.SetEndValues(_t1);

                //Sales Transactions processing
                if (_earliestStartDateTime != DateTime.MinValue)
                {
                    IEnumerable<SalesTransaction> test = dbService.GetSalesTransactionsForMultipleTanks(_siteId, _ownerId, _tankIds, _earliestStartDateTime, _recDateTimeUTC);

                    dbService.GetSalesTransactionsForMultipleTanks(_siteId, _ownerId, _tankIds, _earliestStartDateTime, _recDateTimeUTC).ToList().ForEach(tx =>
                    {
                        //Check if time period available and then get sales
                        if ((tx.EndUTC >= _testPeriod1.StartUTC && tx.EndUTC <= _testPeriod1.EndUTC) || (tx.StartUTC >= _testPeriod1.StartUTC && tx.StartUTC <= _testPeriod1.EndUTC))
                        {
                            _testPeriod1.AddLinkedTanksSales(tx.StartUTC, tx.EndUTC, tx.Quantity);
                        }

                        if ((tx.EndUTC >= _testPeriod2.StartUTC && tx.EndUTC <= _testPeriod2.EndUTC) || (tx.StartUTC >= _testPeriod2.StartUTC && tx.StartUTC <= _testPeriod2.EndUTC))
                        {
                            _testPeriod2.AddLinkedTanksSales(tx.StartUTC, tx.EndUTC, tx.Quantity);
                        }

                        if ((tx.EndUTC >= _testPeriod3.StartUTC && tx.EndUTC <= _testPeriod3.EndUTC) || (tx.StartUTC >= _testPeriod3.StartUTC && tx.StartUTC <= _testPeriod3.EndUTC))
                        {
                            _testPeriod3.AddLinkedTanksSales(tx.StartUTC, tx.EndUTC, tx.Quantity);
                        }
                    });
                }

                //Test Period 1
                _testPeriod1.Variance = _testPeriod1.EndReading - _testPeriod1.StartReading + _testPeriod1.Sales;

                //Test Period 2
                _testPeriod2.Variance = _testPeriod2.EndReading - _testPeriod2.StartReading + _testPeriod2.Sales;

                //Test Period 3
                _testPeriod3.Variance = _testPeriod3.EndReading - _testPeriod3.StartReading + _testPeriod3.Sales;
                
                worstVar = Math.Min(Math.Min(_testPeriod1.Variance, _testPeriod2.Variance), _testPeriod3.Variance);

                return new ReconciliationResult(lastSL.FirstOrDefault(), _brand, worstVar, _testPeriod1, _testPeriod2, _testPeriod3);
            }
        }
    }
}
