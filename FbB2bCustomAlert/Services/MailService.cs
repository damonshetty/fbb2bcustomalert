﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using FbB2bCustomAlert.Models;

namespace FbB2bCustomAlert.Services
{
    public class MailService
    {
        private readonly string _smtpHost;
        private readonly int _smtpPort;
        private readonly string _mailFrom;

        public string MailTo { get; set; }
        public string MailSubject { get; set; }

        readonly StringBuilder _sB;

        public MailService(string smtpHost, int smtpPort, string mailFrom)
        {
            _smtpHost = smtpHost;
            _smtpPort = smtpPort;
            _mailFrom = mailFrom;
            _sB = new StringBuilder();
        }

        public void SendMail(string mailBody)
        {

            MailMessage mail = new MailMessage(_mailFrom, MailTo)
            {
                Subject = MailSubject,
                IsBodyHtml = true,
                Body = mailBody
            };

            new SmtpClient(_smtpHost, _smtpPort) { DeliveryMethod = SmtpDeliveryMethod.Network }.Send(mail);
        }

        //No Data
        public string BuildHtmlNoDataMail(Site site, string reportName, DateTime recTimeUTC, string tankId, string grade)
        {
            _sB.Append($"<div style='font-family:Arial; font-size:14px;'>");

            _sB.Append($"<p>Dear Customer, </p>");
            _sB.Append($"<p>We have not received sufficient data from <strong>{grade} Tanks {tankId}</strong> from <strong>{site.Name}</strong> ({site.DisplayId}) and therefore cannot reconcile report '{reportName}' at this stage.</p>");
            _sB.Append($"<p>Report '{reportName}' will resume once data again becomes available.</p>");
            _sB.Append($"<p>Kind Regards</p>");
            _sB.Append($"<p>Dover Fueling Solutions.</p>");

            return _sB.ToString();
        }

        //Resume Data
        public string BuildHtmResumeMail(Site site, string reportName, DateTime recTimeUTC, string tankIds, string grade, string resumeDataMailSubject)
        {
            _sB.Append($"<div style='font-family:Arial; font-size:14px;'>");

            _sB.Append($"<p>Dear Customer, </p>");
            _sB.Append($"<p>We have now begun receiving data for <strong>{site.Name}</strong> ({site.DisplayId}) <strong>{grade} Tanks {tankIds}</strong> and therefore can resume reconciling '{resumeDataMailSubject}'.</p>");
            _sB.Append($"<p>Kind Regards</p>");
            _sB.Append($"<p>Dover Fueling Solutions.</p>");

            return _sB.ToString();
        }

        //Error
        public string BuildHtmlErrorMail(Site site, DateTime recTimeUTC, string tankIds, string grade, Exception ex)
        {
            _sB.Append($"<div style='font-family:Arial; font-size:14px;'>");

            _sB.Append($"<p>Error generating report for site {site.SiteId} - Tanks {tankIds}.</p>");
            _sB.Append($"<p>Run time UTC: { recTimeUTC.ToString("yyyy-MM-dd HH:mm:ss") }.</p>");
            _sB.Append($"<p>Exception message: {ex.Message}.</p>");
            _sB.Append($"<p>Exception stacktrace: {ex.StackTrace}");

            return _sB.ToString();
        }

        public string BuildHtmlMailBodyForMergedMultipleTanks(ReconciliationResult recResult)
        {
            _sB.Append($"<div style='font-family:Arial; font-size:14px;'>");

            _sB.Append($"<div><strong>Worst variance: {recResult.Variance}</strong></div>");
            _sB.Append("<div>&nbsp;</div>");

            _sB.Append("<table border='1' cellpadding='15'>");
            _sB.Append("<tr><th>Start Local Time</th><th>Tanks</th><th>Open</th><th>End Local Time</th><th>Close</th><th>Sales</th><th>Variance</th></tr>");

            recResult.TestPeriodsLinkedTanks.OrderByDescending(pn => pn.Key).ToList().ForEach(p =>
            {
                if (p.Value != default(TestPeriodLinkedTanks))
                {
                    _sB.Append("<tr>");
                    _sB.Append($"<td>{p.Value.StartLocal.Value.ToString("yyyy-MM-dd HH:mm:ss")}</td>");

                    _sB.Append($"<td>{p.Value.TankId}</td>");
                    _sB.Append($"<td>{Math.Round(p.Value.StartReading, 0)}</td>");

                    _sB.Append($"<td>{p.Value.EndLocal.Value.ToString("yyyy-MM-dd HH:mm:ss")}</td>");
                    _sB.Append($"<td>{Math.Round(p.Value.EndReading, 0)}</td>");
                    _sB.Append($"<td>{Math.Round(p.Value.Sales, 0)}</td>");
                    _sB.Append($"<td>{Math.Round(p.Value.Variance, 2)}</td>");
                    _sB.Append("</tr>");
                }
            });

            _sB.Append("</table></div>");

            return _sB.ToString();
        }

    }
}
