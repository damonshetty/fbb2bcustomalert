﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using FbB2bCustomAlert.Models;

namespace FbB2bCustomAlert.Services
{
    class DBService: IDisposable
    {
        private readonly OracleConnection _conn;

        public DBService(string connString)
        {
            _conn = new OracleConnection(connString);
        }

        private OracleCommand GetSiteCmd(string siteId, string ownerId)
        {
            return new OracleCommand(@"select st_site_id,
                                              ow_owner_id,
                                              st_display_id,
                                              st_name
                                        from COM.SITE 
                                        where st_site_id = :SITEID
                                        and ow_owner_id = :OWNERID", _conn)
            {
                BindByName = true,
                Parameters = { { "SITEID", siteId }, { "OWNERID", ownerId } }
            };
        }

        public Site GetSite(string siteId, string ownerId)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = GetSiteCmd(siteId, ownerId))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                return reader.Read()
                    ? new Site()
                    {
                        SiteId = reader["st_site_id"].ToString(),
                        OwnerId = reader["ow_owner_id"].ToString(),
                        DisplayId = reader["st_display_id"].ToString(),
                        Name = reader["st_name"].ToString(),
                    }
                    : default(Site);
            }
        }

        private OracleCommand LastStockReadingCmd(string siteId, string ownerId, string tankId)
        {
            return new OracleCommand(@"select st_site_id,
                                                   ow_owner_id,
                                                   tk_id,
                                                   sl_date,    
                                                   date_funcs.CastUTCToSite(st_site_id,ow_owner_id,sl_date) localtime
                                            from COM.TIMED_STOCK_LEVEL 
                                            where st_site_id = :SITEID
                                            and ow_owner_id = :OWNERID
                                            and tk_id = :TKID 
                                            and vl_vessel_id = '0000000000'
                                            and sl_date = (select max(sl_date) from timed_stock_level t 
                                                           where t. st_site_id = :SITEID
                                                           and t.ow_owner_id = :OWNERID
                                                           and t.tk_id = :TKID
                                                           and vl_vessel_id = '0000000000')", _conn)
            {
                BindByName = true,
                Parameters = { { "SITEID", siteId }, { "OWNERID", ownerId }, { "TKID", tankId } }
            };
        }

        public StockReading GetLastStockReading(string siteId, string ownerId, string tankId)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = LastStockReadingCmd(siteId, ownerId, tankId))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                return reader.Read()
                        ? new StockReading()
                        {
                            SiteId = reader["st_site_id"].ToString(),
                            OwnerId = reader["ow_owner_id"].ToString(),
                            TankId = reader["tk_id"].ToString(),
                            ReadingDateTimeUTC = Convert.ToDateTime(reader["sl_date"]),
                            ReadingDateTimeLocal = Convert.ToDateTime(reader["localtime"])
                        }
                        : default(StockReading);
            }
        }

        private OracleCommand LastStockReadingMultipleTanksCmd(string siteIds, string ownerId, string tankIds, DateTime recDateTimeUTC)
        {
            StringBuilder sBQuery = new StringBuilder();

            sBQuery.Append("select a.*,t.sl_reading from ( ");

            sBQuery.Append("select st_site_id, ");
            sBQuery.Append("ow_owner_id, ");
            sBQuery.Append("tk_id, ");
            sBQuery.Append("max(sl_date) sl_date, ");
            sBQuery.Append("date_funcs.CastUTCToSite(st_site_id,ow_owner_id,max(sl_date)) localtime, ");
            sBQuery.Append("t.vl_vessel_id ");

            sBQuery.Append("from COM.TIMED_STOCK_LEVEL  t ");
            sBQuery.Append("where t.st_site_id = '" + siteIds + "' ");
            sBQuery.Append("and t.ow_owner_id = '" + ownerId + "' ");
            sBQuery.Append("and t.tk_id in (" + tankIds + ") ");
            sBQuery.Append("and vl_vessel_id = '0000000000' ");
            sBQuery.Append("and sl_date >= to_date('" + recDateTimeUTC.AddHours(-1) + "', 'dd/mm/yyyy hh24:mi:ss')");
            sBQuery.Append("and sl_date <= to_date('" + recDateTimeUTC + "', 'dd/mm/yyyy hh24:mi:ss')");

            sBQuery.Append("group by st_site_id, ");
            sBQuery.Append("ow_owner_id, ");
            sBQuery.Append("tk_id, ");
            sBQuery.Append("vl_vessel_id ");

            sBQuery.Append(") a ");
            sBQuery.Append("inner join timed_stock_level t on t.st_site_id = a.st_site_id ");
            sBQuery.Append("and t.ow_owner_id = a.ow_owner_id ");
            sBQuery.Append("and t.tk_id = a.tk_id ");
            sBQuery.Append("and t.vl_vessel_id = a.vl_vessel_id ");
            sBQuery.Append("and t.sl_date = a.sl_date");

            OracleCommand cmd = GetCommand(sBQuery.ToString());

            return cmd;
        }

        public IEnumerable<StockReading> GetLastStockReadingMultipleTanks(string siteIds, string ownerId, string tankIds, DateTime recDateTimeUTC)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = LastStockReadingMultipleTanksCmd(siteIds, ownerId, tankIds, recDateTimeUTC))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new StockReading()
                    {
                        SiteId = reader["st_site_id"].ToString(),
                        OwnerId = reader["ow_owner_id"].ToString(),
                        TankId = reader["tk_id"].ToString(),
                        ReadingDateTimeUTC = Convert.ToDateTime(reader["sl_date"]),
                        ReadingDateTimeLocal = Convert.ToDateTime(reader["localtime"]),
                        Reading = Convert.ToDouble(reader["sl_reading"])

                    };
                }
            }
        }

        private OracleCommand StockReadingsCmd(string siteId, string ownerId, string tankId, DateTime recDateTimeUTC)
        {
            return new OracleCommand(@"select  ST_SITE_ID,
                                               OW_OWNER_ID,
                                               TK_ID,
                                               SL_DATE,
                                               localtime,
                                               SL_READING,
                                               GP
                                        from 
                                           (select SL_DATE,
                                                   max(SL_DATE) over (partition by GP ) keyreading,
                                                   SL_READING,
                                                   gp,
                                                   localtime,
                                                   ST_SITE_ID,
                                                   OW_OWNER_ID,
                                                   TK_ID
                                            from 
                                                (select SL_DATE,end1,
                                                        (case when SL_DATE<end1-3/24 then 's3'  -- 3 hours
                                                              when SL_DATE<end1-2/24 then 's2'  -- 2 hours
                                                              when SL_DATE<end1-1/24 then 's1'  -- 1 hour

                                                              else 'e1' end) gp,

                                                        SL_READING,
                                                        localtime,
                                                        ST_SITE_ID,
                                                        OW_OWNER_ID,
                                                        TK_ID
                                                 from 
                                                     (select SL_DATE,
                                                             MAX(SL_DATE) over () end1,
                                                             SL_READING,
                                                             date_funcs.CastUTCToSite(ST_SITE_ID,OW_OWNER_ID,SL_DATE) localtime,
                                                             ST_SITE_ID,
                                                             OW_OWNER_ID,
                                                             TK_ID
                                                      from COM.TIMED_STOCK_LEVEL 
                                                      where ST_SITE_ID = :SITEID
                                                        and OW_OWNER_ID = :OWNERID
                                                        and TK_ID = :TKID 
                                                        and SL_DATE > :RECTIMEUTC - (6/ 24) -- 5 hours
                                                        and SL_DATE <= :RECTIMEUTC
                                                        and vl_vessel_id = '0000000000'
                                                     )
                                                )
                                            ) where SL_DATE = keyreading", _conn)
            {
                BindByName = true,
                Parameters = { { "SITEID", siteId }, { "OWNERID", ownerId }, { "TKID", tankId }, { "RECTIMEUTC", recDateTimeUTC } }
            };
        }

        public IEnumerable<StockReading> GetStockReadings(string siteId, string ownerId, string tankId, DateTime recDateTimeUTC)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = StockReadingsCmd(siteId, ownerId, tankId, recDateTimeUTC))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new StockReading()
                    {
                        SiteId = reader.GetString(0),
                        OwnerId = reader.GetString(1),
                        TankId = reader.GetString(2),
                        ReadingDateTimeUTC = reader.GetDateTime(3),
                        ReadingDateTimeLocal = reader.GetDateTime(4),
                        Reading = reader.GetDouble(5),
                        Period = reader.GetString(6)
                    };
                }
            }
        }

        //Get Stock Readings for Multiple Tanks
        private OracleCommand StockReadingsForMultipleTanksCmd(string siteId, string ownerId, string tankIds, DateTime recDateTimeUTC)
        {
            StringBuilder sBQuery = new StringBuilder();

            sBQuery.Append("select TK_ID, SL_DATE, localtime, SL_READING, GP from ");
            sBQuery.Append("(select SL_DATE,TK_ID,max(SL_DATE) over (partition by GP,TK_ID ) keyreading,SL_READING,gp,localtime from ");
            sBQuery.Append("(select SL_DATE,TK_ID,end1, ");
            sBQuery.Append("(case when SL_DATE<end1-3/24 then 's3' ");
            sBQuery.Append("when SL_DATE<end1-2/24 then 's2' ");
            sBQuery.Append("when SL_DATE<end1-1/24 then 's1' ");
            sBQuery.Append("else 'e1' end) gp,  ");
            sBQuery.Append("SL_READING,  ");
            sBQuery.Append("localtime from  ");
            sBQuery.Append("(select SL_DATE,TK_ID,MAX(SL_DATE) over () end1,SL_READING,date_funcs.CastUTCToSite(ST_SITE_ID,OW_OWNER_ID,SL_DATE) localtime ");
            sBQuery.Append("from COM.TIMED_STOCK_LEVEL  ");

            sBQuery.Append("where ST_SITE_ID = '" + siteId + "' ");
            sBQuery.Append("and OW_OWNER_ID = '" + ownerId + "' ");
            sBQuery.Append("and TK_ID in (" + tankIds + ") ");
            sBQuery.Append("and SL_DATE > to_date('" + recDateTimeUTC.AddHours(-5) + "','DD/MM/YY HH24:MI:SS') ");
            sBQuery.Append("and SL_DATE <= to_date('" + recDateTimeUTC + "','DD/MM/YY HH24:MI:SS') ");
            sBQuery.Append("and vl_vessel_id = '0000000000' ");
            sBQuery.Append(") ");
            sBQuery.Append(") ");
            sBQuery.Append(") where SL_DATE = keyreading ");

            OracleCommand cmd = GetCommand(sBQuery.ToString());

            return cmd;
        }

        public IEnumerable<StockReading> GetStockReadingsForMultipleTanks(string siteId, string ownerId, string tankIds, DateTime recDateTimeUTC)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = StockReadingsForMultipleTanksCmd(siteId, ownerId, tankIds, recDateTimeUTC))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new StockReading()
                    {
                        SiteId = siteId,
                        OwnerId = ownerId,
                        TankId = reader.GetString(0),
                        ReadingDateTimeUTC = reader.GetDateTime(1),
                        ReadingDateTimeLocal = reader.GetDateTime(2),
                        Reading = reader.GetDouble(3),
                        Period = reader.GetString(4)
                    };
                }
            }
        }

        private OracleCommand SalesTransactionsCmd(string siteId, string ownerId, string tankId, DateTime txStartTime, DateTime txEndTime)
        {
            return new OracleCommand(@"with config as (
                                              select pp_id, nz_id
                                              from nozzle n inner join tank_nozzle tn
                                                            on tn.tn_id = n.tn_id and
                                                               tn.tn_effective_date = n.tn_effective_date
                                              where tn.st_site_id = :SITEID
                                              and tn.ow_owner_id = :OWNERID 
                                              and tn.tk_id = :TKID
                                              and tn.tn_effective_date = (select max(tn2.tn_effective_date) from tank_nozzle tn2
                                                                          where tn2.st_site_id = tn.st_site_id and
                                                                                tn2.ow_owner_id = tn.ow_owner_id)
                                            )
                                        select t.st_site_id, t.ow_owner_id, t.sx_start, t.sx_end, t.sx_qty
                                        from timed_sales_transaction t inner join config 
                                                                                  on config.pp_id = t.pp_id
                                                                                 and config.nz_id = t.nz_id
                                        where t.st_site_id = :SITEID and 
                                              t.ow_owner_id = :OWNERID and 
                                              t.sx_end > :TSTART and
                                              t.sx_end < :TEND
                                        order by t.sx_end", _conn)
            {
                BindByName = true,
                Parameters = { { "SITEID", siteId }, { "OWNERID", ownerId }, { "TKID", tankId }, { "TSTART", txStartTime }, { "TEND", txEndTime } }
            };
        }

        public IEnumerable<SalesTransaction> GetSalesTransactions(string siteId, string ownerId, string tankId, DateTime startTime, DateTime txEndTime)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = SalesTransactionsCmd(siteId, ownerId, tankId, startTime, txEndTime))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new SalesTransaction()
                    {
                        SiteId = reader.GetString(0),
                        OwnerId = reader.GetString(1),
                        StartUTC = reader.GetDateTime(2),
                        EndUTC = reader.GetDateTime(3),
                        Quantity = reader.GetDouble(4)
                    };
                }
            }
        }

        //Sales Transactions for Multiple Tanks
        private OracleCommand SalesTransactionsForMultipleTanksCmd(string siteId, string ownerId, string tankIds, DateTime txStartTime, DateTime txEndTime)
        {
            StringBuilder sBQuery = new StringBuilder();

            sBQuery.Append("with config as ( ");
            sBQuery.Append("select pp_id, nz_id, tn.tk_id ");
            sBQuery.Append("from nozzle n inner join tank_nozzle tn ");
            sBQuery.Append("on tn.tn_id = n.tn_id and ");
            sBQuery.Append("tn.tn_effective_date = n.tn_effective_date and ");
            sBQuery.Append("tn.st_site_id = '" + siteId + "' ");
            sBQuery.Append("and tn.ow_owner_id = '" + ownerId + "' ");
            sBQuery.Append("and tn.tk_id in (" + tankIds + ") ");
            sBQuery.Append("and tn.tn_effective_date = (select max(tn2.tn_effective_date) from tank_nozzle tn2 ");
            sBQuery.Append("where tn2.st_site_id = tn.st_site_id and ");
            sBQuery.Append("tn2.ow_owner_id = tn.ow_owner_id)) ");
            sBQuery.Append("select t.st_site_id, t.ow_owner_id, t.sx_start, t.sx_end, t.sx_qty, config.tk_id ");
            sBQuery.Append("from timed_sales_transaction t inner join config ");
            sBQuery.Append("on config.pp_id = t.pp_id ");
            sBQuery.Append("and config.nz_id = t.nz_id ");
            sBQuery.Append("where t.st_site_id = '" + siteId + "' and ");
            sBQuery.Append("t.ow_owner_id = '" + ownerId + "' and  ");
            sBQuery.Append("t.sx_end > to_date('" + txStartTime + "','dd/mm/yyyy hh24:mi:ss') and ");
            sBQuery.Append("t.sx_end < to_date('" + txEndTime + "', 'dd/mm/yyyy hh24:mi:ss') ");
            sBQuery.Append("order by t.sx_end ");

            OracleCommand cmd = GetCommand(sBQuery.ToString());

            return cmd;
        }
        public IEnumerable<SalesTransaction> GetSalesTransactionsForMultipleTanks(string siteId, string ownerId, string tankIds, DateTime startTime, DateTime txEndTime)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = SalesTransactionsForMultipleTanksCmd(siteId, ownerId, tankIds, startTime, txEndTime))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new SalesTransaction()
                    {
                        SiteId = reader.GetString(0),
                        OwnerId = reader.GetString(1),
                        StartUTC = reader.GetDateTime(2),
                        EndUTC = reader.GetDateTime(3),
                        Quantity = reader.GetDouble(4),
                        TankId = reader.GetString(5)
                    };
                }
            }
        }
        private OracleCommand FuelGradeCmd(string siteId, string ownerId, string tankId, DateTime effectiveDate)
        {

            return new OracleCommand(@"select fg.fg_id,                             
            nvl(gb.fg_description, fg.fg_description) as fg_description,
            nvl(gb.fg_short_description, fg.fg_short_description) as fg_short_description,
            nvl(gb.fg_text_colour, fg.fg_text_colour) as fg_text_colour,
            nvl(gb.fg_background, fg.fg_background) as fg_background,
            nvl2(gb.fg_text_colour,
            com.web_funcs.rgb_to_web_hex(gb.fg_text_colour),
            com.web_funcs.rgb_to_web_hex(fg.fg_text_colour)) as fg_text_colour_hex,
            nvl2(gb.fg_background,
            com.web_funcs.rgb_to_web_hex(gb.fg_background),
            com.web_funcs.rgb_to_web_hex(fg.fg_background)) as fg_background_hex
            from site st
            inner
            join tank tk
            on tk.st_site_id = st.st_site_id
            and tk.ow_owner_id = st.ow_owner_id
            inner join fuel_code fc
            on fc.ow_owner_id = tk.ow_owner_id
            and fc.st_site_id = tk.st_site_id
            and fc.vl_vessel_id = tk.vl_vessel_id
            and fc.tk_id = tk.tk_id
            inner join lu_fuel_grade fg
            on fc.fg_id = fg.fg_id
            left outer join lu_fuel_grade_brand gb
            on gb.fg_id = fg.fg_id
            and gb.brand_id = st.st_brand_id
            where tk.st_site_id = :SITEID
            and tk.ow_owner_id = :OWNERID
            and tk.vl_vessel_id = '0000000000'
            and tk.tk_id = :TKID
            and fc.fc_effective_date =
            (select max(fc_effective_date)
            from fuel_code
            where st_site_id = fc.st_site_id
            and ow_owner_id = fc.ow_owner_id
            and vl_vessel_id = fc.vl_vessel_id
            and tk_id = fc.tk_id
            and fc_effective_date <= :EFFECTIVEDATE)", _conn)

            {
                BindByName = true,
                Parameters = { { "SITEID", siteId }, { "OWNERID", ownerId }, { "TKID", tankId }, { "EFFECTIVEDATE", effectiveDate } }
            };

        }

        public IEnumerable<FuelType> GetFuelGrade(string siteId, string ownerId, string tankId, DateTime startTime)
        {
            if (_conn.State != System.Data.ConnectionState.Open)
                _conn.Open();

            using (OracleCommand cmd = FuelGradeCmd(siteId, ownerId, tankId, startTime))
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    yield return new FuelType
                    {
                        Grade = reader.GetString(1)
                    };

                }
            }
        }

        private OracleCommand GetCommand(string query)
        {
            return new OracleCommand(@query, _conn);
        }

        public void Dispose()
        {
            _conn.Close();
            _conn.Dispose();
        }

    }
}
