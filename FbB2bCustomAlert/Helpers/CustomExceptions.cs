﻿using System;

namespace FbB2bCustomAlert.Helpers
{
    [Serializable]
    public class StockReadingNotFoundException : Exception
    {
        public StockReadingNotFoundException()
        {
        }
        
        public StockReadingNotFoundException(string message) : base(message)
        {
        }

        public StockReadingNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }


    [Serializable]
    public class SiteNotFoundException : Exception
    {
        public SiteNotFoundException()
        {
        }

        public SiteNotFoundException(string message) : base(message)
        {
        }

        public SiteNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    [Serializable]
    public class DeliveriesNotFoundException : Exception
    {
        public DeliveriesNotFoundException()
        {
        }

        public DeliveriesNotFoundException(string message) : base(message)
        {
        }

        public DeliveriesNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
    [Serializable]
    public class BadRequestException : Exception
    {
        public BadRequestException()
        {
        }

        public BadRequestException(string message) : base(message)
        {
        }

        public BadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
