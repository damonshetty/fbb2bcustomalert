﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FbB2bCustomAlert.Models;
using System.Net.Http;

namespace FbB2bCustomAlert.Helpers
{
    class ClearLastRunDateTime
    {
        //Get current directory
        private static readonly string _saveLastRunDateTimePath = Directory.GetCurrentDirectory() + "\\";
        private static string _textFileName;

        public static void FromTextfile(string siteId, string tankId)
        {
            _textFileName = siteId + "-" + tankId + ".txt";

            if (File.Exists(_saveLastRunDateTimePath + _textFileName))
            {
                //Clear text file contents
                File.WriteAllText(_saveLastRunDateTimePath + _textFileName, "");
            }
        }
    }
}
