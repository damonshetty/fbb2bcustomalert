﻿using System;
using System.IO;
using System.Linq;

namespace FbB2bCustomAlert.Helpers
{
    class ShouldSendResumeEmail
    {
        private static DateTime _lastRunDateTime;
        private static string _textFileName;
        private static bool _checkDateTimeExists;
        public static string _saveLastRunDateTimePath { get; } = Directory.GetCurrentDirectory() + "\\";

        public static bool Check(string siteId, string tankIds)
        {
            if(GetLastDateTimeRun(siteId, tankIds) != DateTime.MinValue)
            {
                ClearLastRunDateTime.FromTextfile(siteId, tankIds);
                return true;
            }
            else
            {
                return false;
            }
        }
        
        //Save DateTime.Now to textfile
        public static void SaveDateTimeToTextfile(string siteId, string tankIds)
        {
            //Clear contents if file exists
            ClearLastRunDateTime.FromTextfile(siteId, tankIds);

            //Write DateTime to textfile
            File.WriteAllText(_saveLastRunDateTimePath + _textFileName, DateTime.Now.ToString());
        }

        //Get last run DateTime from file
        public static DateTime GetLastDateTimeRun(string siteIds, string tankIds)
        {
            _textFileName = siteIds + "-" + tankIds + ".txt";

            //Check contains DateTime
            if (CheckFileExists(tankIds))
            {
                //File contains DateTime
                if (CheckDateTimeExists(tankIds))
                {
                    _lastRunDateTime = Convert.ToDateTime(File.ReadLines(_saveLastRunDateTimePath + _textFileName).FirstOrDefault());
                }
                //File is blank
                else
                {
                    return DateTime.MinValue;
                }
            }

            return _lastRunDateTime;
        }

        //Check file exists
        private static bool CheckFileExists(string tankIds)
        {
            //If file exists
            if (File.Exists(_saveLastRunDateTimePath + _textFileName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Check DateTime witin existing file exist
        private static bool CheckDateTimeExists(string tankIds)
        {
            if (CheckFileExists(tankIds))
            {
                var DateTimeExists = File.ReadLines(_saveLastRunDateTimePath + _textFileName).FirstOrDefault();

                if (DateTimeExists == null)
                {
                    _checkDateTimeExists = false;
                }
                else
                {
                    _checkDateTimeExists = true;
                }
            }

            return _checkDateTimeExists;
        }
    }
}
