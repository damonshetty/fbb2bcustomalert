﻿using System.IO;
using FbB2bCustomAlert.Models;
using Newtonsoft.Json;

namespace FbB2bCustomAlert.Helpers
{
    class DeserializeJSONForMultipleSitesAndTanks
    {
        private string jsonPath;
        private string jsonDeserialize;
        private SitesAndTanksIds sitesnAndTanksIds;
        public DeserializeJSONForMultipleSitesAndTanks()
        {
            sitesnAndTanksIds = new SitesAndTanksIds();

            jsonPath = Directory.GetCurrentDirectory() + "\\Configurations\\" + "SitesAndLinkedMultipleTanksConfiguration.json";
        }

        public SitesAndTanksIds GetDotNetObject()
        {
            using (StreamReader sR = new StreamReader(jsonPath))
            {
                jsonDeserialize = sR.ReadToEnd();
            }

            sitesnAndTanksIds = JsonConvert.DeserializeObject<SitesAndTanksIds>(jsonDeserialize);

            return sitesnAndTanksIds;
        }
    }
}
