﻿using System;
using FbB2bCustomAlert.Models;

namespace FbB2bCustomAlert.Helpers
{
    public static class Variances
    {
        public static void Compare(ref ReconciliationResult recresult1, ref ReconciliationResult recresult2)
        {
            //Compare values and use maximum of the two
            if (recresult1.TestPeriodsLinkedTanks[1].Variance < recresult2.TestPeriodsLinkedTanks[1].Variance)
            {
                recresult1.TestPeriodsLinkedTanks[1] = recresult2.TestPeriodsLinkedTanks[1];
            }

            if (recresult1.TestPeriodsLinkedTanks[2].Variance < recresult2.TestPeriodsLinkedTanks[2].Variance)
            {
                recresult1.TestPeriodsLinkedTanks[2] = recresult2.TestPeriodsLinkedTanks[2];
            }

            if (recresult1.TestPeriodsLinkedTanks[3].Variance < recresult2.TestPeriodsLinkedTanks[3].Variance)
            {
                recresult1.TestPeriodsLinkedTanks[3] = recresult2.TestPeriodsLinkedTanks[3];
            }

            recresult1.Variance = Math.Round(Convert.ToDouble(Math.Min(Math.Min(recresult1.TestPeriodsLinkedTanks[1].Variance, recresult1.TestPeriodsLinkedTanks[2].Variance), recresult1.TestPeriodsLinkedTanks[3].Variance)), 2);
        }

    }
}
