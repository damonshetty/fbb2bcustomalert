﻿using FbB2bCustomAlert.Models;

namespace FbB2bCustomAlert.Helpers
{
    class SetUpValues
    {
        //Set up available variables for error email in case of database error
        public static void FromJson(ref SitesAndTanksIds sitesAndTanksIds, ref string siteNameIfNoDB, ref string siteIdIfNoDB, ref string tankIdsIfNoDB, ref string sitesAndTankIdsIfNoDB)
        {
            foreach(var site in sitesAndTanksIds.Sites)
            {
                siteNameIfNoDB = siteNameIfNoDB + site.Sitename + ", ";
                siteIdIfNoDB = siteIdIfNoDB + site.SiteId + ", ";

                foreach(var tankId in site.TankIds)
                {
                    tankIdsIfNoDB = tankIdsIfNoDB + tankId + ", ";
                }

                sitesAndTankIdsIfNoDB = sitesAndTankIdsIfNoDB + site.Sitename + " Tanks " + ConvertListToCommaDelimitedString<string>.WithoutSingleQuotes(site.TankIds) + ". ";
            }

            siteNameIfNoDB = siteNameIfNoDB.Substring(0, siteNameIfNoDB.Length - 2);
            siteIdIfNoDB = siteIdIfNoDB.Substring(0, siteIdIfNoDB.Length - 2);
            tankIdsIfNoDB = tankIdsIfNoDB.Substring(0, tankIdsIfNoDB.Length - 2);
        }
    }
}
