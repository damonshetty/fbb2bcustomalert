﻿using System.Collections.Generic;
using System.Linq;

namespace FbB2bCustomAlert.Helpers
{
    public static class ConvertCommaDelimitedString
    {
        private static List<string> _convertedList = new List<string>();

        public static List<string> ToListOfTypeString(string commaDelimitedString)
        {
            _convertedList = commaDelimitedString.Replace("'", "").Split(',').ToList();
            
            return _convertedList;
        }
    }
}