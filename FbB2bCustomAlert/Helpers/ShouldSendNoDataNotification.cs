﻿using System;
using System.IO;

namespace FbB2bCustomAlert.Helpers
{
    class ShouldSendNoDataNotification
    {
        private static DateTime _lastRunDateTimeTextFile;

        public static string SaveLastRunDateTimePath { get; } = Directory.GetCurrentDirectory() + "\\";

        //Check if should send no data notification
        public static bool Check(string siteId, string tankIds)
        {
            //Need to get last DateTime run for site and tank id
            _lastRunDateTimeTextFile = ShouldSendResumeEmail.GetLastDateTimeRun(siteId, tankIds);

            //First check for no date. If no DateTime then return true
            if (_lastRunDateTimeTextFile == DateTime.MinValue)
            {
                ShouldSendResumeEmail.SaveDateTimeToTextfile(siteId, tankIds);

                return true;
            }

            //Suppress notifications for 1 hour
            if ((DateTime.Now - _lastRunDateTimeTextFile).Hours >= 0.98)
            {
                ClearLastRunDateTime.FromTextfile(siteId, tankIds);
                ShouldSendResumeEmail.SaveDateTimeToTextfile(siteId, tankIds);

                return true;
            }
            //If 1 hour has not elasped then don't send notification
            else
            {
                return false;
            }
        }
    }
}
