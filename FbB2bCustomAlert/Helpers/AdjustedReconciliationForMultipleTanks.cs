﻿using FbB2bCustomAlert.Models;
using FbB2bCustomAlert.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FbB2bCustomAlert.Helpers
{
    class AdjustedDateTimeReconciliationForMultipleTanks
    {
        private DBService _dbService;
        private DateTime _latestStockReadingDateTimeForMultipleTanks;
        private DateTime _latestStockReadingDateTimeForMultipleTanksAdjusted;

        public AdjustedDateTimeReconciliationForMultipleTanks(DBService dbService)
        {
            _dbService = dbService;
        }

        public DateTime GetDateTimeOfLastStockReading(string siteId, string ownerId, string tankIds, DateTime reconciliationDateTimeUTC, Int16 latestStockReadingDateTimeForMultipleTanksAdjustment, Int16 stockReadingsDelayThreshold)
        {
            //Get latest stocks readings for each tank
            IEnumerable<StockReading> latestStockReadingsForMultipleTanks = _dbService.GetLastStockReadingMultipleTanks(siteId, ownerId, tankIds, reconciliationDateTimeUTC.AddMinutes(-stockReadingsDelayThreshold));

            //Get the latest out of the list of readings
            _latestStockReadingDateTimeForMultipleTanks = latestStockReadingsForMultipleTanks.Select(s => s.ReadingDateTimeUTC).Max();

            //Add 15 seconds
            _latestStockReadingDateTimeForMultipleTanksAdjusted = _latestStockReadingDateTimeForMultipleTanks.AddSeconds(latestStockReadingDateTimeForMultipleTanksAdjustment);

            return _latestStockReadingDateTimeForMultipleTanksAdjusted;
        }

    }
}
