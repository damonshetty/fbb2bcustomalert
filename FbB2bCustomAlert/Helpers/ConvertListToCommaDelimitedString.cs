﻿using System.Collections.Generic;

namespace FbB2bCustomAlert.Helpers
{
    static class ConvertListToCommaDelimitedString<T>
    {
        private static string concatenatedString = "";
        public static string WithSingleQuotes(List<T> list)
        {
            concatenatedString = "'" + string.Join("','", list) + "'";

            return concatenatedString;
        }

        public static string WithoutSingleQuotes(List<T> list)
        {
            concatenatedString = string.Join(", ", list);
            return concatenatedString;
        }
    }
}