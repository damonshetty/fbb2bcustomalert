﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class TestPeriodLinkedTanks
    {
        public DateTime? StartUTC { get; set; } // start
        public DateTime? EndUTC { get; set; }  // end
        public DateTime? StartLocal { get; set; }
        public DateTime? EndLocal { get; set; }
        public double StartReading { get; set; }
        public double EndReading { get; set; }
        public string TankId { get; set; }
        public double Sales { get; set; }
        public double Variance { get; set; } //=> EndReading - StartReading + Sales; // var
        public double StartReadingMerged { get; set; }
        public double EndReadingMerged { get; set; }
        private DateTime _endDateTimeForLinkedTank;


        
        public TestPeriodLinkedTanks()
        {
            Variance = EndReading - StartReading + Sales;
        }

        // addSales
        public void AddSales(DateTime start, DateTime end, double qty)
        {
            if (StartUTC == null || EndUTC == null)
                return;

            //Calculate proportion of sale overlapping period
            double prop = Math.Min(EndUTC.Value.Ticks, end.Ticks) - Math.Max(StartUTC.Value.Ticks, start.Ticks);

            if (start >= end)
                prop = start > StartUTC && !(start > EndUTC) ? 1 : 0; //Covering case of unexpected data
            else
                prop /= end.Ticks - start.Ticks;

            if (prop > 0) Sales += prop * qty;
        }
        
        /*
         * //Need calculation modifcation for the linked tanks
         * 
         * This does not use the end time.
         * 
         * This uses the start time + quantity/120 (assume delivery rate of 120lpm)
         */

        //Add Sales for linked tanks
        public void AddLinkedTanksSales(DateTime start, DateTime end, double qty)
        {
            if (StartUTC == null || EndUTC == null)
                return;

            _endDateTimeForLinkedTank = start + TimeSpan.FromMinutes(qty / 120);

            //Use end DateTime earlier
            if(end < _endDateTimeForLinkedTank)
            {
                _endDateTimeForLinkedTank = end;
            }

            //Calculate proportion of sale overlapping period
            double prop = Math.Min(EndUTC.Value.Ticks, _endDateTimeForLinkedTank.Ticks) - Math.Max(StartUTC.Value.Ticks, start.Ticks);

            if (start >= _endDateTimeForLinkedTank)
                prop = start > StartUTC && !(start > EndUTC) ? 1 : 0; //Covering case of unexpected data
            else
                prop /= _endDateTimeForLinkedTank.Ticks - start.Ticks;

            if (prop > 0) Sales += prop * qty;

        }

        public void SetStartValues(IEnumerable<StockReading> sl)
        {
            this.StartUTC = sl.Select(s => s.ReadingDateTimeUTC).Min(); //     sl.ReadingDateTimeUTC;
            this.StartLocal = sl.Select(s=>s.ReadingDateTimeLocal).Min();
            this.StartReading = sl.Select(s=>s.Reading).Sum();
        }

        public void SetEndValues(IEnumerable<StockReading> sl)
        {
            this.EndUTC = sl.Select(s => s.ReadingDateTimeUTC).Min();
            this.EndLocal = sl.Select(s => s.ReadingDateTimeLocal).Min();
            this.EndReading = sl.Select(s => s.Reading).Sum();
        }

        public override string ToString() =>
             $"{StartLocal.Value.ToString("yyyy-MM-dd HH:mm:ss")}, {StartReading}, {EndLocal.Value.ToString("yyyy-MM-dd HH:mm:ss")}, {EndReading}, {Sales}, {Variance}";
    }
}
