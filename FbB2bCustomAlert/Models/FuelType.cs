﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class FuelType
    {
        public string Grade { get; set; }
        public string Brand { get; set; }
    }
}
