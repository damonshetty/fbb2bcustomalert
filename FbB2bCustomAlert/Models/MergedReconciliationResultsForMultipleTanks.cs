﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class MergedReconciliationResultsForMultipleTanks
    {
        //This class is to merge multiple tanks for linked tanks

        /*
         * Start local time
         * Tank
         * Open
         * End local time
         * Close
         * Sales
         * Variance
         * 
         * 
         */

        public DateTime StartLocalTime { get; set; }
        public string Tank { get; set; }
        public List<double> OpenStockReading { get; set; }


        public double AdjustedOpenStockReadings { get; set; }


        public DateTime EndLocalTime { get; set; }
        public List<double> CloseStockReading { get; set; }


        public double AdjustedCloseStockReadings { get; set; }

        public double Sales { get; set; }
        public double Variance { get; set; }

        public List<OpenAndCloseStockLevels> listOpenAndCloseStockLevels { get; set; }

    }

    public class OpenAndCloseStockLevels
    {
        public double MergedOpenStockReadings { get; set; }
        public double MergedCloseStockReadings { get; set; }

    }
}
