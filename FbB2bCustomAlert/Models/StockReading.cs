﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class StockReading
    {
        public string SiteId { get; set; }
        public string OwnerId { get; set; }
        public string TankId { get; set; }
        public DateTime ReadingDateTimeUTC { get; set; }
        public DateTime ReadingDateTimeLocal { get; set; }
        public string Period { get; set; } //GP
        public double Reading { get; set; }
    }
}
