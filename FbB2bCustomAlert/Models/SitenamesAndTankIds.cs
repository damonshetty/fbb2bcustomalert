﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FbB2bCustomAlert.Models;

namespace FbB2bCustomAlert.Models
{
    //.net object model of the json file
    public class SitenamesAndTankIds
    {
        public string Sitename { get; set; }
        public string SiteId { get; set; }
        public List<string> TankIds { get; set; }
        public int Threshold { get; set; }
        //public List<int> Thresholds { get; set; }
    }

    public class SitesAndTanksIds
    {
        public List<SitenamesAndTankIds> Sites { get; set; }
    }
}
