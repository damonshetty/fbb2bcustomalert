﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class ReconciliationResult
    {
        private readonly double _variance;
        public StockReading LastStockReading { get; }


        public double Variance { get; set; }


        //public double Variance => Math.Round(_variance, 2);
        public Dictionary<int, TestPeriodLinkedTanks> TestPeriodsLinkedTanks { get; }
        public List<ReconciliationResult> ListReconcilicationResults;
        public FuelType Brand;

        public ReconciliationResult(StockReading lastStockReading, FuelType brand, double var = 0, TestPeriodLinkedTanks tp1 = null, TestPeriodLinkedTanks tp2 = null, TestPeriodLinkedTanks tp3 = null)
        {
            LastStockReading = lastStockReading;
            _variance = var;
            Variance = Math.Round(_variance, 2);

            TestPeriodsLinkedTanks = new Dictionary<int, TestPeriodLinkedTanks>()
            {
                {3, tp3}, {2, tp2}, {1, tp1}
            };

            Brand = brand;
        }
        
        public override string ToString()
        {
            return $"Worst Variance: {Variance}{Environment.NewLine}{Environment.NewLine}" +
                   $"Start Local Time       Open       End Local Time      Close     Sales       Variance{Environment.NewLine}" +
                   $"{TestPeriodsLinkedTanks[3]}{Environment.NewLine}{TestPeriodsLinkedTanks[2]}{Environment.NewLine}{TestPeriodsLinkedTanks[1]}{Environment.NewLine}";
        }
    }
}
