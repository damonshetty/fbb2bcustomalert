﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class SalesTransaction
    {
        public string SiteId { get; set; }
        public string OwnerId { get; set; }
        public DateTime StartUTC { get; set; }
        public DateTime EndUTC { get; set; }
        public double Quantity { get; set; }
        public string TankId { get; set; }
    }
}
