﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FbB2bCustomAlert.Models
{
    public class Site
    {
        public string SiteId { get; set; }
        public string OwnerId { get; set; }
        public string DisplayId { get; set; }
        public string Name { get; set; }
    }
}
