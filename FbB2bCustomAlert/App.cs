﻿using FbB2bCustomAlert.Helpers;
using FbB2bCustomAlert.Models;
using FbB2bCustomAlert.Services;
using System;
using System.Configuration;

namespace FbB2bCustomAlert
{
    class App
    {
        private string _connectionString;
        private string _smtpHost;
        private int _smtpPort;
        private string _mailFrom;

        private string _siteId;
        private string _siteIdIfNoDB;
        private string _siteName;
        private string _siteNameIfNoDB;
        private string _tankIdsIfNoDB;
        private string _sitesAndTankIdsIfNoDB;
        private string _grade;
        
        private string _displayId;
        private string _ownerId;
        private string _linkedTankIds;
        private string _linkedTankIdsWithoutQuotes;
        private DateTime _reconciliationDateTimeUTC;
        private Int16 _stockReadingDelayThreshold;
        private Int16 _latestStockReadingDateTimeForMultipleTanksAdjustment;
        private DateTime _latestStockReadingDateTimeForMultipleTanksAdjusted;

        private int _recResultPreviousXXMMinuteComparisonAdjustment;

        private string _reconResultsMailTo;
        private string _reconResultsMailSubject;

        private string _noDataMailTo;
        private string _noDataMailSubject;

        private string _resumeDataMailTo;
        private string _resumeDataMailSubject;
        private string _resumeDataMailBodyReport;

        private string _thresholdBreachMailTo;
        private string _thresholdBreachMailSubject;

        private string _errorMailTo;
        private string _errorMailSubject;

        private Site _site;

        private SitesAndTanksIds sitesAndTanksIds;

        private DeserializeJSONForMultipleSitesAndTanks deserializeJSONForMultipleSitesAndTanks;

        //private DateTime _lastRunDateTimeIfNoDB;

        public App()
        {
            GetAppSettings();

            deserializeJSONForMultipleSitesAndTanks = new DeserializeJSONForMultipleSitesAndTanks();
            _site = new Site();
            _reconciliationDateTimeUTC = DateTime.UtcNow;
            _siteNameIfNoDB = "";
            _siteIdIfNoDB = "";
            _tankIdsIfNoDB = "";
            //_lastRunDateTimeIfNoDB = new DateTime();
            _grade = "Grade N/A";
        }

        internal void Run()
        {
            try
            {
                GetAppSettings();
                _reconciliationDateTimeUTC = DateTime.UtcNow;
                Console.WriteLine("Reconciliation started");

                //Site Id's are from the json file
                sitesAndTanksIds = deserializeJSONForMultipleSitesAndTanks.GetDotNetObject();

                //Set up variables available from json file in case of database error so can send some information in email
                SetUpValues.FromJson(ref sitesAndTanksIds, ref _siteNameIfNoDB, ref _siteIdIfNoDB, ref _tankIdsIfNoDB, ref _sitesAndTankIdsIfNoDB);

                _site.DisplayId = _displayId;
                _site.OwnerId = _ownerId;

                //_lastRunDateTimeIfNoDB = ShouldSendResumeEmail.GetLastDateTimeRun(_siteIdIfNoDB, _tankIdsIfNoDB);

                //Iterate through sites and tanks and concatenate results
                using (var dbService = new DBService(_connectionString))
                {
                    //Test get last stock readings for multiple tanks
                    foreach (var site in sitesAndTanksIds.Sites)
                    {
                        _siteName = site.Sitename;
                        _siteId = site.SiteId;
                        _site = dbService.GetSite(site.SiteId, _ownerId);

                        _linkedTankIds = ConvertListToCommaDelimitedString<string>.WithSingleQuotes(site.TankIds);
                        _linkedTankIdsWithoutQuotes = ConvertListToCommaDelimitedString<string>.WithoutSingleQuotes(site.TankIds);

                        /*
                         * Get adjusted reconciliation time
                         * 
                         * Here we need to find the appropriate reconcilation time for the multiple linked tanks
                         * 
                         * This involves finding the latest reading over the linked multiple tanks and adding 15 seconds
                         * 
                         * Then take the last reading on each tank before this time
                         * 
                         */

                        //Check config if need to use a specific reconciliation DateTime
                        if (bool.Parse(ConfigurationManager.AppSettings["useReconciliationDateTimefromConfig"]))
                        {
                            _latestStockReadingDateTimeForMultipleTanksAdjusted = DateTime.ParseExact(ConfigurationManager.AppSettings["reconciliationDateTimeUTC"], "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            _reconciliationDateTimeUTC = _latestStockReadingDateTimeForMultipleTanksAdjusted;
                        }
                        else
                        {
                            AdjustedDateTimeReconciliationForMultipleTanks adjustedDateTimeReconciliationForMultipleTanks = new AdjustedDateTimeReconciliationForMultipleTanks(dbService);

                            _latestStockReadingDateTimeForMultipleTanksAdjusted = adjustedDateTimeReconciliationForMultipleTanks.GetDateTimeOfLastStockReading(_siteId, _ownerId, _linkedTankIds, 
                                _reconciliationDateTimeUTC, _latestStockReadingDateTimeForMultipleTanksAdjustment, _stockReadingDelayThreshold);

                            _reconciliationDateTimeUTC = _latestStockReadingDateTimeForMultipleTanksAdjusted;
                        }

                        if (_site == default(Site))
                            throw new SiteNotFoundException($"The site of provided id: {_siteId}-{_siteName} doesn't exist");

                        ReconciliationResult recResult = default(ReconciliationResult);
                        ReconciliationResult recResultPreviousXXMMinuteComparison = default(ReconciliationResult);

                        try
                        {
                            recResult = new ReconciliationServiceLinkedTanks(_connectionString, site.SiteId, _ownerId, _linkedTankIds, _reconciliationDateTimeUTC)
                                                .Run();

                            recResultPreviousXXMMinuteComparison = new ReconciliationServiceLinkedTanks(_connectionString, site.SiteId, _ownerId, _linkedTankIds, _reconciliationDateTimeUTC.AddMinutes(-_recResultPreviousXXMMinuteComparisonAdjustment))
                                                .Run();

                            //Each time a reconcilation is made, do the same calculation 10 mintues earlier. Take the max of two calculations. 
                            //This is to avoid if the end time of the interal is the start of a new delivery, artifact can lead to an apparent loss
                            Variances.Compare(ref recResult, ref recResultPreviousXXMMinuteComparison);

                            _grade = recResult.Brand == default(FuelType) ? "Grade N/A" : recResult.Brand.Grade;

                            try
                            {
                                //No Data Found
                                if (recResult.LastStockReading == null ||
                                            recResult.LastStockReading.ReadingDateTimeUTC < DateTime.UtcNow.AddMinutes(-_stockReadingDelayThreshold-5))
                                {
                                    throw new StockReadingNotFoundException();
                                }

                                //Threshold breach
                                else if (recResult.TestPeriodsLinkedTanks[3].Variance < site.Threshold)
                                {
                                    //Resume email
                                    if (ShouldSendResumeEmail.Check(_siteId, _linkedTankIdsWithoutQuotes))
                                    {
                                        BuildAndSendResumeDataEmail(_reconciliationDateTimeUTC, " " + _site.Name + " " + _siteId, _linkedTankIdsWithoutQuotes, _grade);
                                    }

                                    BuildAndSendThresholdBreachEmail(recResult);
                                    Log(_reconciliationDateTimeUTC, recResult, "Reconciliation Threshold Breach. Notification Sent.");
                                }
                                else
                                {
                                    /*
                                     * Reconciliation OK
                                     */

                                    //Resume email
                                    if (ShouldSendResumeEmail.Check(_siteId, _linkedTankIdsWithoutQuotes))
                                    {
                                        BuildAndSendResumeDataEmail(_reconciliationDateTimeUTC, " " + _site.Name + " " + _siteId, _linkedTankIdsWithoutQuotes, _grade);
                                    }

                                    BuildAndSendMergedReconciliationResultsEmail(recResult, _site.Name);
                                    Log(_reconciliationDateTimeUTC, recResult, "Reconcilitation Result OK");
                                }
                            }

                            #region Exceptions
                            catch (StockReadingNotFoundException ex)
                            {
                                //Only send no data email if the last run date is greater than an hour from current DateTime
                                if (ShouldSendNoDataNotification.Check(_siteId, _linkedTankIdsWithoutQuotes))
                                {
                                    BuildAndSendNoDataEmail(_reconciliationDateTimeUTC, _site.Name, _linkedTankIdsWithoutQuotes, _grade);
                                }

                                BuildAndSendErrorEmail(_reconciliationDateTimeUTC, _site.Name + " " + _site.SiteId, _linkedTankIdsWithoutQuotes, _grade, ex);
                                Log(_reconciliationDateTimeUTC, ex, "No Data Exception Sent");
                            }
                            catch (Exception ex)
                            {
                                //Only send no data email if the last run date is greater than an hour from current DateTime
                                if (ShouldSendNoDataNotification.Check(_siteId, _linkedTankIdsWithoutQuotes))
                                {
                                    BuildAndSendNoDataEmail(_reconciliationDateTimeUTC, _site.Name, _linkedTankIdsWithoutQuotes, _grade);
                                }

                                BuildAndSendErrorEmail(_reconciliationDateTimeUTC, _site.Name + " " + _site.SiteId, _linkedTankIdsWithoutQuotes, _grade, ex);
                                Log(_reconciliationDateTimeUTC, ex, "No Data Exception Sent");
                            }
                            #endregion
                        }

                        catch (Exception ex)
                        {
                            //Only send no data email if the last run date is greater than an hour from current DateTime
                            if (ShouldSendNoDataNotification.Check(_siteId, _linkedTankIdsWithoutQuotes))
                            {
                                BuildAndSendNoDataEmail(_reconciliationDateTimeUTC, _site.Name, _linkedTankIdsWithoutQuotes, _grade);
                            }

                            BuildAndSendErrorEmail(_reconciliationDateTimeUTC, _site.Name + " " + _site.SiteId, _linkedTankIdsWithoutQuotes, _grade, ex);
                            Log(_reconciliationDateTimeUTC, ex, "No Data Exception Sent");
                        }

                    }//Foreach site

                }
            }
            catch (SiteNotFoundException ex)
            {
                foreach (var site in sitesAndTanksIds.Sites)
                {
                    _tankIdsIfNoDB = ConvertListToCommaDelimitedString<string>.WithoutSingleQuotes(site.TankIds);
                    _site.Name = site.Sitename;
                    _site.SiteId = site.SiteId;

                    BuildAndSendErrorEmail(_reconciliationDateTimeUTC, site.Sitename + " " + site.SiteId, _tankIdsIfNoDB, _grade, ex);

                    //Only send no data email if the last run date is greater than an hour from current DateTime
                    if (ShouldSendNoDataNotification.Check(site.SiteId, _tankIdsIfNoDB))
                    {
                        BuildAndSendNoDataEmail(_reconciliationDateTimeUTC, site.Sitename + " " + site.SiteId, _tankIdsIfNoDB, _grade);
                    }
                }

                Log(_reconciliationDateTimeUTC, ex, "Site Not Found Notification Sent");
            }
            catch (Exception ex)
            {
                foreach (var site in sitesAndTanksIds.Sites)
                {
                    _tankIdsIfNoDB = ConvertListToCommaDelimitedString<string>.WithoutSingleQuotes(site.TankIds);
                    _site.Name = site.Sitename;
                    _site.SiteId = site.SiteId;

                    BuildAndSendErrorEmail(_reconciliationDateTimeUTC, site.Sitename + " " + site.SiteId, _tankIdsIfNoDB, _grade, ex);

                    //Only send no data email if the last run date is greater than an hour from current DateTime
                    if (ShouldSendNoDataNotification.Check(site.SiteId, _tankIdsIfNoDB))
                    {
                        BuildAndSendNoDataEmail(_reconciliationDateTimeUTC, site.Sitename + " " + site.SiteId, _tankIdsIfNoDB, _grade);
                    }
                }
                Log(_reconciliationDateTimeUTC, ex, "Exception Notification Sent");
            }

        }

        #region Emails

        //Recon Results Email This is a test for merge
        internal void BuildAndSendMergedReconciliationResultsEmail(ReconciliationResult reconciliationResults, string siteName)
        {
            var mailService = new MailService(_smtpHost, _smtpPort, _mailFrom)
            {
                MailTo = _reconResultsMailTo,
                MailSubject = _reconResultsMailSubject + " " + siteName + " Linked Tanks " + _linkedTankIdsWithoutQuotes
            };

            mailService.SendMail(mailService.BuildHtmlMailBodyForMergedMultipleTanks(reconciliationResults));
        }

        //No Data Email
        internal void BuildAndSendNoDataEmail(DateTime recTimeUTC, string siteName, string tankIds, string grade)
        {
            var mailService = new MailService(_smtpHost, _smtpPort, _mailFrom)
            {
                MailTo = _noDataMailTo,
                MailSubject = _noDataMailSubject + " " + siteName + " Tanks " + tankIds
            };

            mailService.SendMail(mailService.BuildHtmlNoDataMail(_site, _noDataMailSubject, recTimeUTC, tankIds, grade));
        }

        //Resume Data Email
        internal void BuildAndSendResumeDataEmail(DateTime recTimeUTC, string siteName, string tankIds, string grade)
        {
            var mailService = new MailService(_smtpHost, _smtpPort, _mailFrom)
            {
                MailTo = _resumeDataMailTo,
                MailSubject = _resumeDataMailSubject + siteName + " Linked Tanks " + tankIds
            };

            mailService.SendMail(mailService.BuildHtmResumeMail(_site, _resumeDataMailTo, recTimeUTC, tankIds, grade, _resumeDataMailBodyReport));
        }

        //Threshold Breach Email
        internal void BuildAndSendThresholdBreachEmail(ReconciliationResult reconciliationResults)
        {
            var mailService = new MailService(_smtpHost, _smtpPort, _mailFrom)
            {
                MailTo = _thresholdBreachMailTo,
                MailSubject = _thresholdBreachMailSubject + " Linked Tanks " + _linkedTankIdsWithoutQuotes
            };

            mailService.SendMail(mailService.BuildHtmlMailBodyForMergedMultipleTanks(reconciliationResults));
        }

        //Error Email
        internal void BuildAndSendErrorEmail(DateTime recTimeUTC, string subject, string tankIds, string grade, Exception ex)
        {
            var mailService = new MailService(_smtpHost, _smtpPort, _mailFrom)
            {
                MailTo = _errorMailTo,
                MailSubject = _errorMailSubject + " " + subject + " Linked Tanks " + tankIds
            };

            mailService.SendMail(mailService.BuildHtmlErrorMail(_site, recTimeUTC, tankIds, grade, ex));
        }

        #endregion

        internal void Log(DateTime runTime, Exception ex, params string[] additionalTexts)
        {
            Console.WriteLine($"Reconciliation datetime UTC: {_reconciliationDateTimeUTC.ToString("yyyy-MM-dd HH:mm:ss")}");
            Console.WriteLine($"Run time UTC: {runTime.ToString("yyyy-MM-dd HH:mm:ss")}");
            Console.WriteLine();
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            Console.WriteLine();
            foreach (var str in additionalTexts)
            {
                Console.WriteLine(str);
                Console.WriteLine();
            }
        }

        internal void Log(DateTime runTime, ReconciliationResult recResult, params string[] additionalTexts)
        {
            Console.WriteLine($"Reconciliation datetime UTC: {_reconciliationDateTimeUTC.ToString("yyyy-MM-dd HH:mm:ss")}");
            Console.WriteLine($"Run time UTC: {runTime.ToString("yyyy-MM-dd HH:mm:ss")}");
            Console.WriteLine();

            foreach (var testPeriod in recResult.TestPeriodsLinkedTanks)
            {
                if (testPeriod.Value.StartUTC != null & testPeriod.Value.EndUTC != null)
                {
                    Console.WriteLine(testPeriod.ToString() + " Tank: " + testPeriod.Value.TankId);
                }
            }

            Console.WriteLine();
            foreach (var str in additionalTexts)
            {
                Console.WriteLine(str);
                Console.WriteLine();
            }
        }

        internal void LogProcessComplete()
        {
            Console.WriteLine("Process complete");
            Console.ReadKey();
        }

        internal void GetAppSettings()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

            if (bool.Parse(ConfigurationManager.AppSettings["useReconciliationDateTimefromConfig"]))
                _reconciliationDateTimeUTC = DateTime.ParseExact(ConfigurationManager.AppSettings["reconciliationDateTimeUTC"], "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            else
                _reconciliationDateTimeUTC = DateTime.UtcNow;

            _stockReadingDelayThreshold = Convert.ToInt16(ConfigurationManager.AppSettings["stockReadingsDelayThreshold"]);
            _latestStockReadingDateTimeForMultipleTanksAdjustment = Convert.ToInt16(ConfigurationManager.AppSettings["maxLatestStockReadingsForMultipleTanksAdjustment"]);
            _recResultPreviousXXMMinuteComparisonAdjustment = Convert.ToInt16(ConfigurationManager.AppSettings["maxLatestStockReadingsForMultipleTanksAdjustment"]);

            _siteName = ConfigurationManager.AppSettings["siteName"];
            _displayId = ConfigurationManager.AppSettings["displayId"];
            _ownerId = ConfigurationManager.AppSettings["ownerId"];

            _smtpHost = ConfigurationManager.AppSettings["smtpHost"];
            _smtpPort = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);
            _mailFrom = ConfigurationManager.AppSettings["mailFrom"];

            _reconResultsMailTo = ConfigurationManager.AppSettings["reconResultsMailTo"];
            _reconResultsMailSubject = ConfigurationManager.AppSettings["reconResultsMailSubject"];

            _noDataMailTo = ConfigurationManager.AppSettings["noDataMailTo"];
            _noDataMailSubject = ConfigurationManager.AppSettings["noDataMailSubject"];

            _resumeDataMailTo = ConfigurationManager.AppSettings["resumeDataMailTo"];
            _resumeDataMailSubject = ConfigurationManager.AppSettings["resumeDataMailSubject"];
            _resumeDataMailBodyReport = ConfigurationManager.AppSettings["resumeDataMailBodyReport"];

            _thresholdBreachMailTo = ConfigurationManager.AppSettings["thresholdBreachMailTo"];
            _thresholdBreachMailSubject = ConfigurationManager.AppSettings["thresholdBreachMailSubject"];

            _errorMailTo = ConfigurationManager.AppSettings["errorMailTo"];
            _errorMailSubject = ConfigurationManager.AppSettings["errorMailSubject"];

        }

    }
}
